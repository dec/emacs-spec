#!/usr/bin/env sh

SPEC_GIT_REVISION=`grep "%define git_revision[^_]" emacs.spec | cut -d " " -f 3`
REMOTE_GIT_REVISION=`git ls-remote https://github.com/emacs-mirror/emacs.git refs/heads/emacs-29 | cut -f 1`

if [ "${SPEC_GIT_REVISION}" = "${REMOTE_GIT_REVISION}" ]
then
    echo "No new git revision : skipping update"
    exit 0
fi

echo "New git revision {${REMOTE_GIT_REVISION}} : updating"

sed -i "s/${SPEC_GIT_REVISION}/${REMOTE_GIT_REVISION}/" emacs.spec
